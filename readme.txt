=== Caorda Site Ripper ===
Contributors: Eric McNiece
Donate link:
Tags:
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

Provides quick download access to various WP directories. Do not leave in site unattended!

== Description ==

Provides quick download access to various WP directories. Do not leave in site unattended!

== Installation ==


== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision
